package edu.ufrn.imd.exemploweb.service;

import java.util.List;

import edu.ufrn.imd.exemploweb.componentes.Mensagem;
import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;
import edu.ufrn.imd.exemploweb.dao.GenericDAO;
import edu.ufrn.imd.exemploweb.dao.HibernateBDException;

public abstract class GenericService<T extends ModeloGenerics> {

	private GenericDAO<T> respository;
	private Mensagem mensagem = new Mensagem();
	
	public GenericService(GenericDAO<T> respository) {
		// TODO Auto-generated constructor stub
		this.respository = respository;
	}

	public void create(T c) {
		respository.create(c);
		mensagem = respository.getMsg();
	}

	public void update(T c) {
		respository.update(c);
		mensagem = respository.getMsg();
	}

	public void delete(T c) {
		respository.delete(c);
		mensagem = respository.getMsg();
	}

	public T findByPrimaryKey(int id) throws HibernateBDException {
		return respository.findByPrimaryKey(id);
	}

	public T findByPrimaryKeyJPQL(int id) throws HibernateBDException {
		// EntityManager em = Database.getInstance().getEntityManager();

		return respository.findByPrimaryKeyJPQL(id);
	}

	public List<T> findAll() throws HibernateBDException {
		return respository.findAll();
	}

	public List<T> findAllLike(String col, String valor) throws HibernateBDException {

		return respository.findAllLike(col, valor);
	}

	public List<T> findAllEqual(String col, String valor) throws HibernateBDException {

		return findAllEqual(col, valor);
	}

	public T findEqual(String col, String valor) throws HibernateBDException {

		return respository.findEqual(col, valor);
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

}
