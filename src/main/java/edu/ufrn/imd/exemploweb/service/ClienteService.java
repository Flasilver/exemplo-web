package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.ClienteDAO;
import edu.ufrn.imd.exemploweb.model.Cliente;

public class ClienteService extends GenericService<Cliente> {

	public ClienteService() {
		super(new ClienteDAO());
		// TODO Auto-generated constructor stub
	}

}
