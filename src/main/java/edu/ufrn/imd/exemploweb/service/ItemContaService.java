package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.ItemContaDAO;
import edu.ufrn.imd.exemploweb.model.ItemConta;

public class ItemContaService extends GenericService<ItemConta>{

	public ItemContaService() {
		// TODO Auto-generated constructor stub
		super(new ItemContaDAO());
	}
}
