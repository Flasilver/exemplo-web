package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.ItemDAO;
import edu.ufrn.imd.exemploweb.model.Item;

public class ItemService extends GenericService<Item> {
    
	public ItemService() {
		super(new ItemDAO());
	}
}
