package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.MesaDAO;
import edu.ufrn.imd.exemploweb.model.Mesa;

public class MesaService extends GenericService<Mesa>{

	public MesaService() {
		super(new MesaDAO());
	}
}
