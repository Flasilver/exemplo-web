package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.ContaDAO;
import edu.ufrn.imd.exemploweb.model.Conta;

public class ContaService  extends GenericService<Conta>{
       
	public ContaService() {
		super(new ContaDAO());
	}
}
