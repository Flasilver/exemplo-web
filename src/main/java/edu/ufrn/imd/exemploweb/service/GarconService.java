package edu.ufrn.imd.exemploweb.service;

import edu.ufrn.imd.exemploweb.dao.GarconDAO;
import edu.ufrn.imd.exemploweb.model.Garcon;

public class GarconService extends GenericService<Garcon>{

//	private GarconRepository garconRepository;
    
    public GarconService() {
    	super(new GarconDAO());
    }
}
