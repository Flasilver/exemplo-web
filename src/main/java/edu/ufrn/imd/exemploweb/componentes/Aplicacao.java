package edu.ufrn.imd.exemploweb.componentes;

import javax.faces.bean.ApplicationScoped;
import javax.faces.component.FacesComponent;

@FacesComponent(value="aplicacao")
@ApplicationScoped
public class Aplicacao {

	private static Aplicacao singleton = 
			new Aplicacao();
	private static final String nomeSistema = "Sistema Bar";
	private static final String versaoSistema = "1.0.0 snapshot";
	
	private Aplicacao(){
	}
	
	public static Aplicacao getInstance(){
		if(singleton == null)
			singleton = new Aplicacao();
		return singleton;
	}

	public static String getVersaoSistema() {
		return versaoSistema;
	}

	public static String getNomeSistema() {
		return nomeSistema;
	}
		
}
