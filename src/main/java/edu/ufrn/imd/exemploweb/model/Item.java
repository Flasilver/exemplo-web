package edu.ufrn.imd.exemploweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.type.descriptor.java.UUIDTypeDescriptor.ToStringTransformer;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class Item implements ModeloGenerics{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column
	private String descricao;
	
	@Column
	private double valor;
	
	@Column
	private String marca;
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof Item)
			if(((Item)obj).getId() == id)
				return true;
		
		return false;
	}
	
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	@Override
	public String toString() {
		return descricao+ "(R$ "+ String.format("%1$,.2f", valor)+ ")";
	}
}
