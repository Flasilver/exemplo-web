package edu.ufrn.imd.exemploweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class ItemConta implements ModeloGenerics{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="conta_id")
	private Conta conta;
	@ManyToOne
	@JoinColumn(name="item_id")
	private Item item;
	@Column(nullable=false)
	private int quantidade;
	
	public ItemConta() {
		// TODO Auto-generated constructor stub
		item = new Item();
		conta = new Conta();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof ItemConta)
			if(((ItemConta)obj).getId() == id)
				return true;
		
		return false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public double getValorTotal(){
		double valor = item != null ? quantidade*item.getValor() : 0;
		return valor;
	}

}
