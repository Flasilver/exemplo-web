package edu.ufrn.imd.exemploweb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class Garcon  extends Pessoa implements ModeloGenerics{

	@Column
	private String matricula;
	@Column
	private double salario;
	@Column
	private String carteiraTrabalho;
	@OneToMany(mappedBy="garconResponsavel")
	List<Conta> contas;
	
	public Garcon() {
		super();
	}
	public Garcon(String nome, String cpf, String endereco, String telefone) {
		super(nome, cpf, endereco, telefone);
	}
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public String getCarteiraTrabalho() {
		return carteiraTrabalho;
	}
	public void setCarteiraTrabalho(String carteiraTrabalho) {
		this.carteiraTrabalho = carteiraTrabalho;
	}
	
	public List<Conta> getContas() {
		return contas;
	}
	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}
	
	public int getTotalContasAtivas() {
		int resultado = 0;
		for (Conta conta : contas) {
				if(!conta.isPago())
					resultado ++;
		}
		return resultado;
	}
}
