package edu.ufrn.imd.exemploweb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class Cliente extends Pessoa implements ModeloGenerics{

	@Column
	private String cartaoFidelidade;
	@Column
	private String credito;
	
	@OneToMany(mappedBy="cliente")
	List<Conta> contas;
	
	public Cliente() {
		super();
		contas = new ArrayList<Conta>();
	}
	public Cliente(String nome, String cpf, String endereco, String telefone) {
		super(nome, cpf, endereco, telefone);
		contas = new ArrayList<Conta>();
	}
	
	public String getCartaoFidelidade() {
		return cartaoFidelidade;
	}
	public void setCartaoFidelidade(String cartaoFidelidade) {
		this.cartaoFidelidade = cartaoFidelidade;
	}
	public String getCredito() {
		return credito;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}

	public List<Conta> getContas() {
		return contas;
	}
	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}
	
	public int getTotalContasAtivas() {
		int resultado = 0;
		for (Conta conta : contas) {
				if(!conta.isPago())
					resultado ++;
		}
		return resultado;
	}
	
	public double getSubTotal() {
		double resultado = 0;
		for (Conta conta : contas) {
			resultado += conta.getSubTotal();
		}
		return resultado;
	}
	
}
