package edu.ufrn.imd.exemploweb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class Conta implements ModeloGenerics{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@OneToMany(mappedBy = "conta", fetch = FetchType.EAGER)
	private List<ItemConta> itens;
	
	@ManyToOne
	@JoinColumn(name="garcon_id")
	private Garcon garconResponsavel;
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name="mesa_id")
	private Mesa mesa;
	@Column
	private Boolean pago;
	
	@Transient
	private double subTotal;
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof Conta)
			if(((Conta)obj).getId() == id)
				return true;
		
		return false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public List<ItemConta> getItens() {
		return itens;
	}
	public void setItens(List<ItemConta> itens) {
		this.itens = itens;
	}
	public Garcon getGarconResponsavel() {
		return garconResponsavel;
	}
	public void setGarconResponsavel(Garcon garconResponsavel) {
		this.garconResponsavel = garconResponsavel;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Mesa getMesa() {
		return mesa;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public double getSubTotal() {
		subTotal = 0; 
		if(itens != null)
			if(itens.size() > 0)
				if(pago == null) {
					for (ItemConta itemConta : itens) 
						if(itemConta.getItem() != null)
							subTotal += itemConta.getValorTotal();
				}
				else
					if(!pago)
						for (ItemConta itemConta : itens) 
							if(itemConta.getItem() != null)
								subTotal += itemConta.getValorTotal();
		
		return subTotal;
	}
	
	public double getSubTotalGeral() {
		subTotal = 0; 
		if(itens != null)
			if(itens.size() > 0)
				for (ItemConta itemConta : itens) 
					if(itemConta.getItem() != null)
						subTotal += itemConta.getValorTotal();
		
		return subTotal;
	}
	
	public boolean isPago() {
		if(pago == null)
			return false;
		
		return (boolean) pago;
	}
	public Boolean getPago() {
		return pago;
	}
	
	public void setPago(boolean pago) {
		this.pago = pago;
	}

}
