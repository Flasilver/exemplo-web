package edu.ufrn.imd.exemploweb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;

@Entity
public class Mesa implements ModeloGenerics{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column
	private int numero;
	
	@OneToMany(mappedBy="mesa")
	private List<Conta> contas;
	
	public Mesa() {
		// TODO Auto-generated constructor stub
		contas = new ArrayList<Conta>();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof Mesa)
			if(((Mesa)obj).getId() == id)
				return true;
		
		return false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}
	
	public int getTotalContasAtivas() {
		int resultado = 0;
		for (Conta conta : contas) { 
				if(!conta.isPago())
					resultado ++;
		}
		return resultado;
	}
	
	public double getSubTotal() {
		double resultado = 0;
		for (Conta conta : contas) {
			resultado += conta.getSubTotal();
		}
		return resultado;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Mesa "+ numero;
	}
}
