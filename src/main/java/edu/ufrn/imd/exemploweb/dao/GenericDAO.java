package edu.ufrn.imd.exemploweb.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;


//import javax.management.Query;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import edu.ufrn.imd.exemploweb.componentes.Mensagem;
import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;



public abstract class GenericDAO <T extends ModeloGenerics> {
	
	protected EntityManager em = ConexaoDatabase.getInstance().getEntityManager();
	private Mensagem msg = new Mensagem();
	
	private void change (T c, OperacaoDatabase op){
		try{
			if(em.isOpen()){
				if(c != null){
					if(!em.getTransaction().isActive())
						em.getTransaction().begin();
					switch (op){
					case INSERIR:
						em.persist(c);
						break;
					case ALTERAR:
						em.merge(c);
						break;
					case REMOVER:
						em.remove(c);
						break;
					}
				}
				
				if(em.getTransaction().isActive()) {
					em.getTransaction().commit();
					switch (op){
					case INSERIR:
						msgInfo(c.getClass().getSimpleName()+ " inserido com sucesso");
						break;
					case ALTERAR:
						msgInfo(c.getClass().getSimpleName()+ " alterado com sucesso");
						break;
					case REMOVER:
						if(c.getId() <= 0)
							msgWarn("Registro de "+ c.getClass().getSimpleName()+ " com id "+ c.getId()+ " n�o encontrado");
						else
							msgInfo(c.getClass().getSimpleName()+ " excluido com sucesso");
						break;
					
					}
				}
			}
		}
		catch(HibernateBDException e){
//			e.printStackTrace();
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();
			msgErro("Erro HibernateBDException:"+ e.getMessage());
//			String msg = e.getErro(e.getCause());
		}
		catch(Exception ex) {
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			ex.printStackTrace(pw);
			pw.flush();
			sw.flush();
			String msg = sw.toString();
			
			msgErro("Erro "+ ex.getClass().getSimpleName()+ " : "+ msg);
		}

	}
	
	
	public void create(T c){
		change(c, OperacaoDatabase.INSERIR);
	}
	
	public void update(T c){
		change(c, OperacaoDatabase.ALTERAR);
	}
	
	public void delete(T c){
		change(c, OperacaoDatabase.REMOVER);
	}
	
	enum OperacaoDatabase{
		INSERIR,ALTERAR,REMOVER
	}	
	
	
	public T findByPrimaryKey(int id) throws HibernateBDException{
		//EntityManager em = Database.getInstance().getEntityManager();
		T c = null;
		if(em.isOpen())
			c = em.find(getClassType(), id);
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public T findByPrimaryKeyJPQL(int id) throws HibernateBDException{
		//EntityManager em = Database.getInstance().getEntityManager();
		T c = null;
		
		if(em.isOpen()){
			
			String tabela = getClassType().getSimpleName();
			String jpql = "from "+ tabela+ " where id = :valor";
			//EntityManager em = Database.getInstance().getEntityManager();
			
			Query query = em.createQuery(jpql);
			query.setParameter("valor", id);
			c = (T) query.getSingleResult();
		}
		
		return c;
	}
	
	public List<T> findAll() throws HibernateBDException{
		//EntityManager em = Database.getInstance().getEntityManager()
		List<T> c = null;
		
		if(em.isOpen()){
			CriteriaBuilder builder =  em.getCriteriaBuilder();
			CriteriaQuery<T> query = builder.createQuery(getClassType());
			TypedQuery<T> typedQuery = em.createQuery(query.select(query.from(getClassType())));
			c = typedQuery.getResultList();
		}
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAllLike(String col, String valor) throws HibernateBDException{
		
		List<T> c = null;
		
		if(em.isOpen()){
			String tabela = getClassType().getSimpleName();
			String jpql = "from "+ tabela+ " where "+ col+ " like :valor";
			//EntityManager em = Database.getInstance().getEntityManager();
			
			Query query = em.createQuery(jpql);
			query.setParameter("valor", "%"+ valor+ "%");
			c = query.getResultList();
		}
		
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAllEqual(String col, String valor) throws HibernateBDException{
		
		List<T> c = null;
		
		if(em.isOpen()){
			String tabela = getClassType().getSimpleName();
			String jpql = "from "+ tabela+ " where "+ col+ " = :valor";
			
			Query query = em.createQuery(jpql);
			query.setParameter("valor", valor);
			c = query.getResultList();
		}
		
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public T findEqual(String col, String valor) throws HibernateBDException{
		
		T c = null;
		
		if(em.isOpen()){
			String tabela = getClassType().getSimpleName();
			String jpql = "from "+ tabela+ " where "+ col+ " = :valor";
			
			Query query = em.createQuery(jpql);
			query.setParameter("valor", valor);
			if( !(query.getResultList().isEmpty()) )
				c = (T) query.getSingleResult();
		}
		
		return c;
	}
	
	public void fecharConexao() throws HibernateBDException{
		em.close();
	}
	
	public abstract Class<T> getClassType();


	public Mensagem getMsg() {
		return msg;
	}


	public void setMsg(Mensagem msg) {
		this.msg = msg;
	}
	
	public void msgWarn(String msgConteudo) {
		msg.setCodigo(2);
		msg.setConteudo(msgConteudo);
	}
	
	public void msgErro(String msgConteudo) {
		msg.setCodigo(1);
		msg.setConteudo(msgConteudo);
	}
	
	public void msgInfo(String msgConteudo) {
		msg.setCodigo(0);
		msg.setConteudo(msgConteudo);
	}

}
