package edu.ufrn.imd.exemploweb.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class ConexaoDatabase {
	private static ConexaoDatabase singleton = 
			new ConexaoDatabase();
	private static EntityManager em;
	
	private ConexaoDatabase(){
		EntityManagerFactory emf = 
				Persistence.createEntityManagerFactory("ConexaoDB");
		em = emf.createEntityManager();
	}
	
	public static ConexaoDatabase getInstance()
			throws HibernateBDException{
		return singleton;
	}
	
	public EntityManager getEntityManager()
			throws HibernateBDException{
		return em;
	}
	
}
