package edu.ufrn.imd.exemploweb.dao;

import edu.ufrn.imd.exemploweb.model.Item;

public class ItemDAO extends GenericDAO<Item>{

	@Override
	public Class<Item> getClassType() {
		// TODO Auto-generated method stub
		return Item.class;
	}
    
}