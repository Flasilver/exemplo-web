package edu.ufrn.imd.exemploweb.dao;

import org.hibernate.HibernateException;

@SuppressWarnings("serial")
public class HibernateBDException extends HibernateException{
	
	public HibernateBDException(Throwable e) {
		// TODO Auto-generated constructor stub
		super(e);
	}
	
	public String getErro(Throwable msg){
		
		String erro = "[Erro"+ msg.hashCode()+ "] : "+ msg.getMessage()+ "\n\n rastro :\n"+ msg.getStackTrace().toString();
		return erro;
	}
	
}
