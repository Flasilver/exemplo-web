package edu.ufrn.imd.exemploweb.webui.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "validatorCPF")
public class ValidatorCPF implements Validator {

	private String cpf;

	@Override
	public void validate(FacesContext context, UIComponent componet, Object value) throws ValidatorException {
		// TODO Auto-generated method stub

		cpf = value.toString();

		if (!numeroDeCaracteres(11) || !digitoVerificador1() || !digitoVerificador2())
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF inv�lido", "CPF deve ser 11 d�gitos num�ricos em ordem v�lida. (Veja em https://www.4devs.com.br/gerador_de_cpf)"));

	}

	private boolean numeroDeCaracteres(int num) {
		return cpf.length() == num;
	}

	private boolean digitoVerificador1() {
		int soma = 0;
		int multiplicador = 10;
		char[] lista = cpf.toCharArray();
		for (char algarismo : lista) {
			soma += multiplicador * Character.getNumericValue(algarismo);
			if (multiplicador-- == 2)
				break;
		}

		double resto = soma % 11;
		int decimoNumero = Character.getNumericValue(lista[9]);
		if (resto == 0 || resto == 1)
			return decimoNumero == 0;
		else
			return decimoNumero == 11 - resto;

	}

	private boolean digitoVerificador2() {
		int soma = 0;
		int multiplicador = 11;
		char[] lista = cpf.toCharArray();
		for (char algarismo : lista) {
			soma += multiplicador * Character.getNumericValue(algarismo);
			if (multiplicador-- == 2)
				break;
		}

		double resto = soma % 11;
		int decimoPrimeiroNumero = Character.getNumericValue(lista[10]);
		if (resto == 0 || resto == 1)
			return decimoPrimeiroNumero == 0;
		else
			return decimoPrimeiroNumero == 11 - resto;
	}
}
