package edu.ufrn.imd.exemploweb.webui;


import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import edu.ufrn.imd.exemploweb.componentes.Aplicacao;
import edu.ufrn.imd.exemploweb.componentes.Mensagem;
import edu.ufrn.imd.exemploweb.dao.HibernateBDException;
import edu.ufrn.imd.exemploweb.model.Conta;
import edu.ufrn.imd.exemploweb.model.Item;
import edu.ufrn.imd.exemploweb.model.ItemConta;
import edu.ufrn.imd.exemploweb.service.ContaService;
import edu.ufrn.imd.exemploweb.service.ItemContaService;
import edu.ufrn.imd.exemploweb.service.ItemService;

@ManagedBean
@SessionScoped
public class ItemContaBean extends GenericBean<ItemConta> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Mensagem mensagem = new Mensagem();
	
	public ItemContaBean() {
		// TODO Auto-generated constructor stub
	
		setService(new ItemContaService());
		super.setListaItens(new ArrayList<ItemConta>());
		super.setItemSelecionado(new ItemConta());
		super.setAplicacao(Aplicacao.getInstance());
		setNomeSistema(getAplicacao().getInstance().getNomeSistema());
		setVersaoSistema( getAplicacao().getInstance().getVersaoSistema());
		recarregarLista();
	}
	
	public void selecionarItemConta(ActionEvent e){

	}
	
	public void salvar(ActionEvent e) {
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;
		
		try {
			getService().create(getItemSelecionado());
			
			switch(getService().getMensagem().getCodigo()) {
				case 1:
					tipoMsg = FacesMessage.SEVERITY_ERROR;
					break;
				case 2 :
					tipoMsg = FacesMessage.SEVERITY_WARN;
					break;
				default:
					tipoMsg = FacesMessage.SEVERITY_INFO;
			}
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, getService().getMensagem().getConteudo()
					, getService().getMensagem().getConteudo()));
			recarregarLista();	
		}
	}
	public void alterar(ActionEvent e) {
		
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;

		try {
			getService().update(getItemSelecionado());
		
			switch(getService().getMensagem().getCodigo()) {
				case 1:
					tipoMsg = FacesMessage.SEVERITY_ERROR;
					break;
				case 2 :
					tipoMsg = FacesMessage.SEVERITY_WARN;
					break;
				default:
					tipoMsg = FacesMessage.SEVERITY_INFO;
			}
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, getService().getMensagem().getConteudo()
					, getService().getMensagem().getConteudo()));
			recarregarLista();			
		}
	}
	public void excluir(ActionEvent e) {
		
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;
		try {
			getService().delete(getItemSelecionado());

			switch(getService().getMensagem().getCodigo()) {
				case 1:
					tipoMsg = FacesMessage.SEVERITY_ERROR;
					break;
				case 2 :
					tipoMsg = FacesMessage.SEVERITY_WARN;
					break;
				default:
					tipoMsg = FacesMessage.SEVERITY_INFO;
			}
			
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, "Erro hibernate "+ getService().getMensagem().getConteudo()
					, "Erro hibernate "+ getService().getMensagem().getConteudo()));
			recarregarLista();			
		}
	
	}
	
	private void recarregarLista() {
		setListaItens(getService().findAll());
		//[Ganbiarra] Para atualizar a lista de pedidos da conta, n�o est� funcionando com um select em conta fetchtype.EAGER
		//Era para funcionar fazendo select de cona denovo, mas n�o funciona.
		Conta contaContaItemSelecionado = new Conta();
		Item itemContaItemSelecionado = new Item();
		if(getItemSelecionado().getConta().getId() > 0) {
			contaContaItemSelecionado = new ContaService().findByPrimaryKey(getItemSelecionado().getConta().getId());
			itemContaItemSelecionado = new ItemService().findByPrimaryKey(getItemSelecionado().getItem().getId());
			getItemSelecionado().setItem(itemContaItemSelecionado);
		}
		
		ItemConta ic = getItemSelecionado();
		setItemSelecionado(new ItemConta());
		if(contaContaItemSelecionado.getId() > 0) {
			//[Ganbiarra] Para atualizar a lista de pedidos da conta, n�o est� funcionando com um select em conta fetchtype.EAGER
			getItemSelecionado().setConta(contaContaItemSelecionado);
			getItemSelecionado().getConta().getItens().add(ic);
		}
	}
	
	public Mensagem getMensagem() {
		return mensagem;
	}
	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
