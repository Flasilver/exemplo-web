package edu.ufrn.imd.exemploweb.webui;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import edu.ufrn.imd.exemploweb.componentes.Aplicacao;
import edu.ufrn.imd.exemploweb.componentes.Mensagem;
import edu.ufrn.imd.exemploweb.dao.HibernateBDException;
import edu.ufrn.imd.exemploweb.model.Conta;
import edu.ufrn.imd.exemploweb.service.ContaService;

@ManagedBean
@SessionScoped
public class ContaBean extends GenericBean<Conta> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Mensagem mensagem = new Mensagem();
	
	public ContaBean() {
		// TODO Auto-generated constructor stub
	
		setService(new ContaService());
		super.setListaItens(new ArrayList<Conta>());
		super.setItemSelecionado(new Conta());
		super.setAplicacao(Aplicacao.getInstance());
		setNomeSistema(getAplicacao().getInstance().getNomeSistema());
		setVersaoSistema( getAplicacao().getInstance().getVersaoSistema());
		recarregarLista();
	}
	
	public void selecionarConta(ActionEvent e){

	}
	
	public void salvar(ActionEvent e) {
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;
		
		try {
			Conta novoConta = new Conta();
//			novoConta.setDescricao(getItemSelecionado().getDescricao());
//			novoConta.setValor(getItemSelecionado().getValor());
//			setContaSelecionado(novoConta);
//			getService().create(getItemSelecionado());
//			
//			switch(getService().getMensagem().getCodigo()) {
//				case 1:
//					tipoMsg = FacesMessage.SEVERITY_ERROR;
//					break;
//				case 2 :
//					tipoMsg = FacesMessage.SEVERITY_WARN;
//					break;
//				default:
//					tipoMsg = FacesMessage.SEVERITY_INFO;
//			}
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, getService().getMensagem().getConteudo()
					, getService().getMensagem().getConteudo()));
			recarregarLista();			
		}
	}
	public void alterar(ActionEvent e) {
		
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;

		try {
			getService().update(getItemSelecionado());
		
			switch(getService().getMensagem().getCodigo()) {
				case 1:
					tipoMsg = FacesMessage.SEVERITY_ERROR;
					break;
				case 2 :
					tipoMsg = FacesMessage.SEVERITY_WARN;
					break;
				default:
					tipoMsg = FacesMessage.SEVERITY_INFO;
			}
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, getService().getMensagem().getConteudo()
					, getService().getMensagem().getConteudo()));
			recarregarLista();			
		}
	}
	public void excluir(ActionEvent e) {
		
		FacesMessage.Severity tipoMsg = FacesMessage.SEVERITY_INFO;
		try {
			getService().delete(getItemSelecionado());

			switch(getService().getMensagem().getCodigo()) {
				case 1:
					tipoMsg = FacesMessage.SEVERITY_ERROR;
					break;
				case 2 :
					tipoMsg = FacesMessage.SEVERITY_WARN;
					break;
				default:
					tipoMsg = FacesMessage.SEVERITY_INFO;
			}
			
		}
		catch(HibernateBDException dbe) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		catch(Exception ex) {
			tipoMsg = FacesMessage.SEVERITY_ERROR;
		}
		finally {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMsg, "Erro hibernate "+ getService().getMensagem().getConteudo()
					, "Erro hibernate "+ getService().getMensagem().getConteudo()));
			recarregarLista();			
		}
	
	}
	
	private void recarregarLista() {
		setListaItens(getService().findAll());
	}
	
	public Mensagem getMensagem() {
		return mensagem;
	}
	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
