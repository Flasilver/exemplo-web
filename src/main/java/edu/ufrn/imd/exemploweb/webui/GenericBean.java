package edu.ufrn.imd.exemploweb.webui;

import java.util.List;

import javax.faces.event.ActionEvent;

import edu.ufrn.imd.exemploweb.componentes.Aplicacao;
import edu.ufrn.imd.exemploweb.componentes.ModeloGenerics;
import edu.ufrn.imd.exemploweb.service.GenericService;

public abstract class GenericBean <T extends ModeloGenerics>{

	private GenericService<T> service;
	private T itemSelecionado;
	private List<T> listaItens;
	private Aplicacao aplicacao;
	private String versaoSistema;
	private String nomeSistema;
	
	public GenericService<T> getService() {
		return service;
	}
	
	public void setService(GenericService<T> service) {
		this.service = service;
	}	

	public abstract void salvar(ActionEvent e);
	public abstract void alterar(ActionEvent e);
	public abstract void excluir(ActionEvent e);
	
	public T getItemSelecionado() {
		return itemSelecionado;
	}

	public void setItemSelecionado(T itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	public List<T> getListaItens() {
		return listaItens;
	}

	public void setListaItens(List<T> listaItens) {
		this.listaItens = listaItens;
	}

	public Aplicacao getAplicacao() {
		return aplicacao;
	}

	public void setAplicacao(Aplicacao aplicacao) {
		this.aplicacao = aplicacao;
	}

	public String getVersaoSistema() {
		return versaoSistema;
	}

	public void setVersaoSistema(String versaoSistema) {
		this.versaoSistema = versaoSistema;
	}

	public String getNomeSistema() {
		return nomeSistema;
	}

	public void setNomeSistema(String nomeSistema) {
		this.nomeSistema = nomeSistema;
	}

}
